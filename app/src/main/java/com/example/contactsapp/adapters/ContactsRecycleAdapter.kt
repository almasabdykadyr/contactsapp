package com.example.contactsapp.adapters

import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.contactsapp.R
import com.example.contactsapp.data.Contact
import com.example.contactsapp.databinding.ContactItemBinding

class ContactsRecycleAdapter :
    ListAdapter<Contact, ContactsRecycleAdapter.ViewHolder>(ContactDiffCallback()) {

    class ViewHolder(private val binding: ContactItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(contact: Contact) = with(binding) {
            contactName.text = contact.name.toString()
            contactPhone.text = contact.phoneNumber.toString()

            if (contact.profilePicture != null) contactAvatar.setImageURI(
                Uri.parse(contact.profilePicture)
            )
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ContactItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}
