package com.example.contactsapp.adapters

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.contactsapp.R
import com.example.contactsapp.data.Contact
import com.example.contactsapp.databinding.ContactItemBinding

class ContactsListAdapter(context: Context, val contacts: List<Contact>) :
    ArrayAdapter<Contact>(context, R.layout.contact_item, contacts) {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val itemView = convertView ?: inflater.inflate(R.layout.contact_item, parent, false)
        val contact = contacts[position]

        val itemName = itemView.findViewById<TextView>(R.id.contact_name)
        val itemPhone = itemView.findViewById<TextView>(R.id.contact_phone)
        val itemPicture = itemView.findViewById<ImageView>(R.id.contact_avatar)


        itemName.text = contact.name
        itemPhone.text = contact.phoneNumber
        if (contact.profilePicture == null) {
            itemPicture.setImageResource(R.drawable.user_svgrepo_com)
        } else itemPicture.setImageURI(Uri.parse(contact.profilePicture!!))
        return itemView
    }
}