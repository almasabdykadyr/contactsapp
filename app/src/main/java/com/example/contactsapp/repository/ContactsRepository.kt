package com.example.contactsapp.repository

import android.content.ContentResolver
import android.provider.ContactsContract
import android.provider.ContactsContract.Contacts
import android.util.Log
import androidx.loader.content.CursorLoader
import com.example.contactsapp.data.Contact
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import kotlin.coroutines.suspendCoroutine

class ContactsRepository(private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO) {

    suspend fun getContactsListAsync(contentResolver: ContentResolver): List<Contact> {
        return withContext(ioDispatcher) {
            getContactsList(contentResolver)
        }
    }

    private fun getContactsList(contentResolver: ContentResolver): List<Contact> {
        val contactsList: MutableList<Contact> = mutableListOf()
        val cursor = contentResolver.query(
            Contacts.CONTENT_URI, null, null, null, null
        )
        if (cursor != null && cursor.count > 0) {
            while (cursor.moveToNext()) {
                val contactId = cursor.getString(cursor.getColumnIndex(Contacts._ID) or 0)
                val contactName =
                    cursor.getString(cursor.getColumnIndex(Contacts.DISPLAY_NAME) or 0)
                val phoneCursor = contentResolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? ",
                    arrayOf<String>(contactId),
                    null
                )

                var contactPhone = ""

                if (phoneCursor != null && phoneCursor.moveToNext()) {
                    contactPhone =
                        phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER) or 0)
                    phoneCursor.close()
                }

                val contactPictureURI =
                    cursor.getString(cursor.getColumnIndex(Contacts.PHOTO_URI) or 0)

                contactsList.add(Contact(contactId, contactName, contactPictureURI, contactPhone))
            }
        }
        cursor?.close()
        return contactsList
    }
}