package com.example.contactsapp.data

data class Contact(
    val id: String?,
    val name: String?,
    val profilePicture: String?,
    val phoneNumber: String?
)
