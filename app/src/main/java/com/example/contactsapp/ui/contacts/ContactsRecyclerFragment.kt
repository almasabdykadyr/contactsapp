package com.example.contactsapp.ui.contacts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.contactsapp.adapters.ContactsRecycleAdapter
import com.example.contactsapp.data.Contact
import com.example.contactsapp.databinding.FragmentContactsRecyclerBinding
import com.example.contactsapp.repository.ContactsRepository
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 * Use the [ContactsRecyclerFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ContactsRecyclerFragment : Fragment() {

    private var _binding: FragmentContactsRecyclerBinding? = null
    private val binding get() = _binding!!
    private val adapter = ContactsRecycleAdapter()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentContactsRecyclerBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.adapter = adapter

        lifecycleScope.launch {
            val contactsRepository = ContactsRepository()
            val contacts = contactsRepository.getContactsListAsync(requireContext().contentResolver)
            adapter.submitList(contacts)
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ContactsRecyclerFragment.
         */
        @JvmStatic
        fun newInstance() = ContactsRecyclerFragment()
    }
}