package com.example.contactsapp.ui.contacts

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import com.example.contactsapp.R
import com.example.contactsapp.adapters.ContactsListAdapter
import com.example.contactsapp.data.Contact
import com.example.contactsapp.databinding.FragmentContactsListBinding
import com.example.contactsapp.repository.ContactsRepository
import kotlinx.coroutines.launch

/**
 * A simple [Fragment] subclass.
 * Use the [ContactsListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ContactsListFragment : Fragment() {

    private var _binding: FragmentContactsListBinding? = null
    private var contacts: List<Contact> = emptyList()

    private lateinit var adapter: ContactsListAdapter
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentContactsListBinding.inflate(inflater, container, false)
        val view = binding.root
        adapter = ContactsListAdapter(requireContext(), contacts)
        binding.listView.adapter = adapter
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch {
            val contactsRepository = ContactsRepository()
            contacts = contactsRepository.getContactsListAsync(requireContext().contentResolver)
        }.invokeOnCompletion {
            adapter.addAll(contacts)
            adapter.notifyDataSetChanged()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment ContactsListFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = ContactsListFragment()

    }
}